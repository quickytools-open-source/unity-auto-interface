using System.Reflection;
using static System.Reflection.BindingFlags;

namespace Quickytools.AutoInterface
{
    public static class Constants
    {
        public const BindingFlags PUBLIC_PROPERTY_FLAGS = Public & GetField & SetField;
        public const BindingFlags NON_PROPERTY_FLAGS = ~(GetProperty | SetProperty);

        public const string AssetsQuickytoolsFilePath = "Assets/Quickytools";
        public const string GeneratedAutoInterfaceDirName = "Generated-AutoInterface";
    }

    public static class StyleClass
    {
        public const string GROUP_CLASS = "ai-group";
        public const string GROUP_LABEL_CLASS = "ai-group-label";
        public const string GROUP_ROW_CLASS = "ai-group-row";

        public const string ACTION_CLASS = "ai-action";
    }
}