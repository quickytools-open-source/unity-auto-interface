﻿using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Quickytools.AutoInterface
{
    public enum ElementType
    {
        Binding,
        Action
    }

    public abstract class AutoInterfaceElement
    {
        private static int sElementIndex = 0;

        /// <summary>
        /// Member or method name the attribute is applied to
        /// </summary>
        public string TargetName { get; private set; }

        /// <summary>
        /// A group name if one was applied
        /// </summary>
        public string GroupName { get; private set; }

        /// <summary>
        /// A group row if one was applied
        /// </summary>
        /// <value></value>
        public int GroupRow { get; private set; }

        /// <summary>
        /// The global order in which the element was created
        /// </summary>
        public int ConstructIndex { get; private set; }

        /// <summary>
        /// The type of element
        /// </summary>
        private ElementType ElementType { get; set; }

        protected AutoInterfaceElement(ElementType elementType, string targetName, string groupName = "", int groupRow = 0)
        {
            ElementType = elementType;
            TargetName = targetName;
            GroupName = groupName ?? "";
            GroupRow = groupRow;
            ConstructIndex = sElementIndex++;
        }

        public abstract VisualElement GetElement(Object editorTarget);
    }

    public class BindingElement : AutoInterfaceElement
    {
        public BindingElement(string targetName, string groupName = "", int groupRow = 0) : base(ElementType.Binding, targetName, groupName, groupRow)
        {
        }

        /// <summary>
        /// Creates the UI element
        /// </summary>
        /// <param name="editorTarget">The Editor target</param>
        /// <returns>UI element</returns>
        public override VisualElement GetElement(Object editorTarget)
        {
            return new PropertyField()
            {
                bindingPath = TargetName,
                name = $"prop_{TargetName.ToLower()}"
            };
        }
    }

    public class ActionElement : AutoInterfaceElement
    {
        /// <summary>
        /// Label of the element if one is applied
        /// </summary>
        public string Label { get; private set; }

        public ActionElement(string targetName, string label, string groupName = "", int groupRow = 0) : base(ElementType.Action, targetName, groupName, groupRow)
        {
            Label = label;
        }

        public override VisualElement GetElement(Object editorTarget)
        {
            var actionUi = new Button() { text = Label };
            actionUi.AddToClassList(StyleClass.ACTION_CLASS);
            actionUi.RegisterCallback<MouseUpEvent>(e =>
            {
                var innerType = editorTarget.GetType();
                var magicMethod = innerType.GetMethod(TargetName, Constants.NON_PROPERTY_FLAGS);
                magicMethod.Invoke(editorTarget, new object[] { });
            });
            return actionUi;
        }
    }
}