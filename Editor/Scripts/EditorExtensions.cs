using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using static Quickytools.AutoInterface.Constants;
using static Quickytools.AutoInterface.StyleClass;

namespace Quickytools.AutoInterface
{
    public static class EditorExtensions
    {
        public static void BuildAutoInterfaceExtension(this Editor editor, TemplateContainer container)
        {
            // Group elements
            var groupedElements = new Dictionary<string, (int, IList<AutoInterfaceElement>)>();
            var type = editor.target.GetType();
            GroupElements(GetBindingElements(type));
            GroupElements(GetActionElements(type));
            void GroupElements(IEnumerable<AutoInterfaceElement> elements)
            {
                foreach (var element in elements)
                {
                    var groupName = element.GroupName ?? "";
                    var index = element.ConstructIndex;
                    if (!groupedElements.ContainsKey(groupName))
                    {
                        groupedElements.Add(groupName, (index, new List<AutoInterfaceElement>()));
                    }
                    var group = groupedElements[groupName];
                    group.Item1 = Math.Min(index, group.Item1);
                    group.Item2.Add(element);
                }
            }

            // Order groups by index of first appearance
            var ordered = groupedElements
                .Select(entry =>
                {
                    var (groupIndex, elements) = entry.Value;
                    return (groupIndex, entry.Key, elements);
                })
                .OrderBy(t => t.groupIndex);

            // Create interface
            foreach (var (groupIndex, group, elements) in ordered)
            {
                // Each group has a container
                var groupElement = new VisualElement();
                groupElement.AddToClassList(GROUP_CLASS);
                container.Add(groupElement);

                // Add group label if specified
                var isGrouped = !string.IsNullOrWhiteSpace(group);
                if (isGrouped)
                {
                    var groupLabel = new Label() { text = group };
                    groupLabel.AddToClassList(GROUP_LABEL_CLASS);
                    groupElement.Add(groupLabel);

                    // TODO Better group naming logic by removing invalid characters
                    var specificGroupClass = $"group-{group.Trim().ToLower().Replace(" ", "-")}";
                    groupElement.AddToClassList(specificGroupClass);
                }

                // Add elements ordered by first appearance
                var groupRowElements = new SortedList<int, VisualElement>();
                foreach (var element in elements.OrderBy(e => e.ConstructIndex))
                {
                    var editorElement = element.GetElement(editor.target);
                    if (!isGrouped)
                    {
                        // TODO How to classify ungrouped elements for custom styling?
                    }

                    var row = element.GroupRow;
                    if (row != 0)
                    {
                        if (!groupRowElements.TryGetValue(row, out var rowElement))
                        {
                            rowElement = new VisualElement();
                            rowElement.AddToClassList(GROUP_ROW_CLASS);
                            var specificRowClass = $"row-{row}";
                            rowElement.AddToClassList(specificRowClass);
                            groupRowElements[row] = rowElement;
                            groupElement.Add(rowElement);
                        }
                        rowElement.Add(editorElement);
                    }
                    else
                    {
                        groupElement.Add(editorElement);
                    }
                }
            };
        }

        private static IEnumerable<AutoInterfaceElement> GetBindingElements(Type type)
        {
            var serializedFields = type.GetMembers(NON_PROPERTY_FLAGS)
                                       .Where(member => member.GetCustomAttribute<SerializeField>() != null)
                                       .Select(ToBindingElement);
            var publicProperties = type.GetMembers(PUBLIC_PROPERTY_FLAGS)
                                       .Select(ToBindingElement);
            BindingElement ToBindingElement(MemberInfo member)
            {
                var groupAttribute = member.GetCustomAttribute<EditorInterfaceGroup>();
                var groupRow = member.GetCustomAttribute<EditorInterfaceGroupRow>();
                return new BindingElement(member.Name, groupAttribute?.GroupName, groupRow?.RowOrder ?? 0);
            };
            var combined = new List<AutoInterfaceElement>(serializedFields);
            combined.AddRange(publicProperties);
            return combined;
        }

        private static IEnumerable<AutoInterfaceElement> GetActionElements(Type type)
        {
            return type.GetMethods(NON_PROPERTY_FLAGS)
                .Where(method => method.GetCustomAttribute<EditorInterfaceAction>() != null)
                .Select(method =>
                {
                    var label = method.GetCustomAttribute<EditorInterfaceAction>().Label;
                    var methodName = method.Name;
                    var groupAttribute = method.GetCustomAttribute<EditorInterfaceGroup>();
                    var groupRow = method.GetCustomAttribute<EditorInterfaceGroupRow>();
                    return new ActionElement(methodName, label, groupAttribute?.GroupName, groupRow?.RowOrder ?? 0);
                });
        }
    }
}