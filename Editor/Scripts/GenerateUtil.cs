using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using static Quickytools.AutoInterface.Constants;

namespace Quickytools.AutoInterface
{
    public static class GenerateUtil
    {
        private static readonly Regex NamespaceRx = new Regex(@"\bnamespace ([^{]+)", RegexOptions.Compiled);

        [UnityEditor.Callbacks.DidReloadScripts]
        public static void RegenerateEditors()
        {
            var scripts = Resources.FindObjectsOfTypeAll<MonoScript>();
            var mbScripts = scripts.Where(s => s.GetClass()?.IsSubclassOf(typeof(MonoBehaviour)) ?? false);

            // TODO Operate over changed scripts only. Take no action if changes do not change any interfaces.

            // Filter for scripts that define auto interfaces
            var interfaceScripts = mbScripts.Where(s =>
            {
                var type = s.GetClass();
                var serializedGroups = type.GetMembers(NON_PROPERTY_FLAGS)
                    .Where(member => member.GetCustomAttribute<SerializeField>() != null &&
                                     member.GetCustomAttribute<EditorInterfaceGroup>() != null);
                if (serializedGroups.Count() > 0)
                {
                    return true;
                }

                var editorActions = type.GetMethods(NON_PROPERTY_FLAGS)
                    .Where(method => method.GetCustomAttribute<EditorInterfaceAction>() != null);
                if (editorActions.Count() > 0)
                {
                    return true;
                }

                return false;
            });

            if (interfaceScripts.Count() > 0)
            {
                CreateInterfaces(interfaceScripts);
                AssetDatabase.Refresh();
            }
        }

        private static void CreateInterfaces(IEnumerable<MonoScript> interfaceScripts)
        {
            // TODO Set a single path depending if loaded as package or local development
            var templatesDirPath = Path.GetFullPath("Packages/com.quickytools.auto-interface/Editor/Templates");
            var devTemplatesPath = "Assets/Quickytools/AutoInterface/Templates";
            if (AssetDatabase.IsValidFolder(devTemplatesPath))
            {
                templatesDirPath = Path.GetFullPath(devTemplatesPath);
            }

            var assetsQuickytoolsPath = AssetsQuickytoolsFilePath;
            var generatedDirName = GeneratedAutoInterfaceDirName;

            var generatedEditorDirRelativePath = $"{generatedDirName}/Editor";
            CreateAssetDirIfNotExist(generatedEditorDirRelativePath);

            var generatedEditorDirPath = $"{assetsQuickytoolsPath}/{generatedEditorDirRelativePath}";
            var directoryInfo = new DirectoryInfo(generatedEditorDirPath);
            var editorFiles = directoryInfo.GetFiles().Select(f => f.Name);

            var baseEditorContents = File.ReadAllText($"{templatesDirPath}/BaseActionsEditor");

            var createdScripts = interfaceScripts.Select(CreateInterface);

            var scriptsSet = new HashSet<string>(createdScripts);
            // Each created script will have a meta as well
            foreach (var s in createdScripts)
            {
                scriptsSet.Add($"{s}.meta");
            }

            // Delete stale editors only, not all.
            var forDelete = editorFiles
                .Where(f => !scriptsSet.Contains(f))
                .Select(f => $"{generatedEditorDirPath}/{f}");
            if (forDelete.Count() > 0)
            {
                Debug.Log($"Delete these stale scripts {string.Join(",", forDelete)}");
                // TODO This won't run when if there's an error in the editor... Redesign for expected functionality.
                // AssetDatabase.DeleteAssets(forDelete.ToArray(), new List<string>());
            }

            string CreateInterface(MonoScript script)
            {
                var className = script.GetClass().Name;
                var lowerName = className.ToLower();

                // Create static resources
                var relativeLayoutPath = $"{generatedDirName}/UI/{lowerName}_inspector.uxml";
                CopyAssetTo($"{templatesDirPath}/base_inspector.uxml", relativeLayoutPath, typeof(VisualTreeAsset), true);
                var relativeStylesPath = $"{generatedDirName}/UI/base_auto_interface.uss";
                CopyAssetTo($"{templatesDirPath}/base_auto_interface.uss", relativeStylesPath, typeof(StyleSheet));

                // Capture namespace if exists
                var code = script.text;
                var matches = NamespaceRx.Match(code);
                var nsBegin = "";
                var nsEnd = "";
                if (matches.Groups.Count > 1)
                {
                    var ns = matches.Groups[1].Value;
                    nsBegin = $"namespace {ns} {{";
                    nsEnd = "}";
                }

                // Create editor referencing script and static resources
                var newLayoutPath = $"{assetsQuickytoolsPath}/{relativeLayoutPath}";
                var newStylesPath = $"{assetsQuickytoolsPath}/{relativeStylesPath}";
                var newEditorContents = baseEditorContents
                    .Replace(@"// EDITOR_NAMESPACE", nsBegin)
                    .Replace(@"// CLOSING_EDITOR_NAMESPACE", nsEnd)
                    .Replace("REPLACE_TYPE", className)
                    .Replace("PATH_TO_LAYOUT", newLayoutPath)
                    .Replace("PATH_TO_STYLES", newStylesPath);
                var editorFileName = $"{className}Editor.cs";
                var newEditorPath = $"{assetsQuickytoolsPath}/{generatedDirName}/Editor/{editorFileName}";
                File.WriteAllText(newEditorPath, newEditorContents);

                return editorFileName;
            }

            void CreateAssetDirIfNotExist(string relativePath)
            {
                var fullPath = $"{assetsQuickytoolsPath}/{relativePath}";
                var parts = fullPath.Split('/');
                var lastPath = "";
                foreach (var partPath in parts)
                {
                    var prefix = lastPath.Length > 0 ? $"{lastPath}/" : "";
                    var newPath = $"{prefix}{partPath}";
                    if (!AssetDatabase.IsValidFolder(newPath))
                    {
                        AssetDatabase.CreateFolder(lastPath, partPath);
                    }
                    lastPath = newPath;
                }
            }

            void CopyAssetTo(string sourceFullFilePath, string destRelativeFilePath, Type type, bool overwrite = false)
            {
                var dest = $"{assetsQuickytoolsPath}/{destRelativeFilePath}";
                if (overwrite || !File.Exists(dest))
                {
                    CreateAssetDirIfNotExist(Path.GetDirectoryName(destRelativeFilePath));
                    FileUtil.ReplaceFile(sourceFullFilePath, dest);
                }
            }
        }
    }
}