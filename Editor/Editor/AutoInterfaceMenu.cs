using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Quickytools.AutoInterface
{
    using static Constants;

    public class AutoInterfaceMenu : MonoBehaviour
    {
        [MenuItem("Tools/quickytools/Auto Interface/Clean Empty Dirs")]
        static void CleanEmptyDirs()
        {
            var emptyGeneratedDirs = GetEmptyDirectories(AssetsQuickytoolsFilePath, GeneratedAutoInterfaceDirName);
            var emptyDirs = new List<string>(emptyGeneratedDirs);
            var dirPath = $"{AssetsQuickytoolsFilePath}/{GeneratedAutoInterfaceDirName}";
            emptyDirs.AddRange(GetEmptyDirectories(dirPath));
            if (emptyDirs.Count() > 0)
            {
                var failed = new List<string>();
                AssetDatabase.DeleteAssets(emptyDirs.ToArray(), failed);
                if (failed.Count > 0)
                {
                    Debug.LogWarning($"Failed to delete {failed.Count} dirs {string.Join(",", failed)}");
                }
            }
        }

        private static IEnumerable<string> GetEmptyDirectories(string dirPath, string matchingDirPrefix = "")
        {
            var directoryInfo = new DirectoryInfo(dirPath);
            return directoryInfo.GetDirectories()
                    .Select(di => IsEmptyDirectory(di, matchingDirPrefix) ? $"{dirPath}/{di.Name}" : "")
                    .Where(s => !string.IsNullOrWhiteSpace(s));
        }

        private static bool IsEmptyDirectory(DirectoryInfo di, string matchingDirPrefix = "")
        {
            var isMatchingName = string.IsNullOrWhiteSpace(matchingDirPrefix) || di.Name.StartsWith(matchingDirPrefix);
            return isMatchingName && di.GetDirectories().Length + di.GetFiles().Length == 0;
        }

        [MenuItem("Tools/quickytools/Auto Interface/Regenerate")]
        static void RegenerateInterfaces()
        {
            var dirPath = $"{AssetsQuickytoolsFilePath}/{GeneratedAutoInterfaceDirName}";
            AssetDatabase.DeleteAssets(new string[] { dirPath }, new List<string>());
        }
    }
}