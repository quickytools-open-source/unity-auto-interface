# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.2] - 2022-05-16
### Fixed
- Namespaced classes generate editor UI correctly by deriving the `UnityEditor.Editor` class.

## [0.2.1] - 2022-01-24
### Added
- `EditorInterfaceGroupRow` annotation for separating groups further into rows.
### Fixed
- Padding of actions for increased hit area.

## [0.2.0] - 2022-01-23
### Changed
- Increase Unity version and remove `com.unity.ui` dependency. v0.1.1 has a lower Unity version.

## [0.1.1] - 2021-10-13
### Changed
- Increase Unity version and update `com.unity.ui` dependency. v0.1.0 has a lower version of the UI dependency.

## [0.0.32] - 2021-02-23
### Fixed
- `[Serialized] public` fields are reflected once in the editor (not twice).

## [0.0.31] - 2021-01-13
### Added
- Public members of auto interface classes are included in generated editor interfaces.
### Fixed
- Added Runtime assembly to accommodate non-Editor builds.

## [0.0.30] - 2020-12-30
### Added
- Menu options under Tools > quickytools > Auto Interface for deleting empty generated directories or regenerating editor files completely.

### Fixed
- Generated editors are under the same namespace as scripts.

## [0.0.28] - 2020-11-09
### Added
- Generates interfaces when detecting various attributes on component methods and/or methods
- AutoInterface attributes
  - `EditorInterfaceAction` adds a button for invoking an action
  - `EditorInterfaceGroup` groups members and/or methods together