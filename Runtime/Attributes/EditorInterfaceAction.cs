using System;

namespace Quickytools.AutoInterface
{
    /// <summary>
    /// Creates a button (in the editor interface) for invoking a method
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class EditorInterfaceAction : Attribute
    {
        private readonly string actionLabel;

        public EditorInterfaceAction(string actionLabel)
        {
            this.actionLabel = actionLabel;
        }

        public virtual string Label => actionLabel;
    }
}