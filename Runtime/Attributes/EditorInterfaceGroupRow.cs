﻿using System;

namespace Quickytools.AutoInterface
{
    /// <summary>
    /// Assigns a row order to a group element for bucketing (into rows)
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Method)]
    public class EditorInterfaceGroupRow : Attribute
    {
        private readonly int rowOrder;

        public EditorInterfaceGroupRow(int rowOrder)
        {
            this.rowOrder = rowOrder;
        }

        public virtual int RowOrder => rowOrder;
    }
}