﻿using System;

namespace Quickytools.AutoInterface
{
    /// <summary>
    /// Defines a group for the member or method to aggregate in an editor interface
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Method)]
    public class EditorInterfaceGroup : Attribute
    {
        private readonly string groupName;

        public EditorInterfaceGroup(string groupName)
        {
            this.groupName = groupName;
        }

        public virtual string GroupName => groupName;
    }
}