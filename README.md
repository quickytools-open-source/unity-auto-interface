# AutoInterface BETA

Generates editors for components with various applied attributes.

Group `SerializeField` members and/or methods with `EditorInterfaceGroup`. Groups have a label and appear in the order at which groups are initially encountered. Divide groups into rows with `EditorInterfaceGroupRow` for better use of layout.

Add actions to invoke methods with `EditorInterfaceAction`. Methods must not accept parameters.

Generated files are located at Assets/Quickytools/Generated-AutoInterface. Files are generated (and overwritten) every time scripts are reloaded so manual changes to these files will be lost.

### Known issues and workarounds
- Renaming components with AutoInterface attributes will break the generation process.  
  *Delete all previously generated auto interface editor files manually.* New files will be generated in place. There are now menu items for deleting empty generated dirs and regenerating files under Tools > quickytools > Auto Interface.
- Layout and style files are not deleted when unnecessary.  
  *Delete any unnecessary files manually.*
- There are no automated tests.  
  *Write automated tests.*
