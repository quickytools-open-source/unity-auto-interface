﻿using System.Collections.Generic;
using UnityEngine;
using Quickytools.AutoInterface;

public class PinSetup : MonoBehaviour
{
    private readonly IList<(Vector3, Quaternion)> initialPoses = new List<(Vector3, Quaternion)>();

    // Start is called before the first frame update
    void Start()
    {
        foreach (Transform pin in transform)
        {
            initialPoses.Add((pin.position, pin.rotation));
        }
    }

    [EditorInterfaceAction("Reset pins")]
    public void ResetPins()
    {
        for (var i = 0; i < transform.childCount; i++)
        {
            var (position, rotation) = initialPoses[i];
            var pin = transform.GetChild(i);
            var pinRb = pin.GetComponent<Rigidbody>();
            pinRb.velocity = Vector3.zero;
            pinRb.angularVelocity = Vector3.zero;
            pin.SetPositionAndRotation(position, rotation);
        }
    }
}
