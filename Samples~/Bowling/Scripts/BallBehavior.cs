﻿using UnityEngine;
using Quickytools.AutoInterface;

public class BallBehavior : MonoBehaviour
{
    [SerializeField]
    GameObject groundPlane = default;

    [SerializeField]
    [EditorInterfaceGroup("Editor Throw")]
    Transform throwDirection = default;

    private Vector3 initialPosition;

    private new Rigidbody rigidbody;

    private float belowGroundPlane;

    void Start()
    {
        belowGroundPlane = groundPlane.transform.position.y - GetComponent<MeshRenderer>().bounds.max.y;

        rigidbody = GetComponent<Rigidbody>();

        initialPosition = transform.position;

        ApplyForce();
    }

    private void FixedUpdate()
    {
        if (transform.position.y < belowGroundPlane)
        {
            var randomOffset = Random.onUnitSphere * 2;
            randomOffset.y = 0;
            transform.position = initialPosition + randomOffset;
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
        }
    }

    [EditorInterfaceGroup("Editor Throw")]
    [EditorInterfaceAction("Throw")]
    private void ApplyForce()
    {

        rigidbody.AddForce(1000 * throwDirection.forward, ForceMode.Impulse);
    }
}
